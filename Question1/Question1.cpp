﻿// Question1.cpp : This file contains the 'main' function. Program execution begins and ends there.
//

#include "pch.h"
#include <iostream>
#include <cmath>
#include <iomanip>
using namespace std;

#define nMin 10
#define nMax 320
#define pMin 1
#define pMax 128

void printHorizontalLine() { // Helper function to print lines
	cout << "\n+";
	for (int i = 0; i < 7; i++) {
		cout << "------+";
	}
	cout << "\n";
}
void Question1() {
	int n, p;
	/*while (true)
	{*/
	cout << "This is the program for the Question 1 of Parallel Programming Assignment 2019 (UCTS) "
		"which is used to calculate the speedups and efficiencies of a parallel program configuration\n" << endl;
	cout << "This particular program will run with parameters:\n" <<
		"n = 10, 20, 40, ... up until 320\n" <<
		"p = 1, 2, 4, ... up until 128\n\n";

	// Speedups
	cout << "Speedups";
	printHorizontalLine();
	cout << "|" << setw(6) << "" << "|";
	for (int i = nMin; i <= nMax; i *= 2)
	{
		cout << setw(6) << i << "|";
	}
	printHorizontalLine();
	for (double p = pMin; p <= pMax; p *= 2) {
		cout << "|" << setw(6) << p << "|";
		for (double n = nMin; n <= nMax; n *= 2)
		{
			double nsquare = n * n;
			double serialTime = nsquare;
			double parallelTime = nsquare / p + log2(p);
			double speedUp = round(serialTime / parallelTime * 100) / 100;
			cout << setw(6) << speedUp << "|";
		}
		printHorizontalLine();
	}

	// Speedups
	cout << "\nParallel Efficiency";
	printHorizontalLine();
	cout << "|" << setw(6) << "" << "|";
	for (int i = nMin; i <= nMax; i *= 2)
	{
		cout << setw(6) << i << "|";
	}
	printHorizontalLine();
	for (double p = pMin; p <= pMax; p *= 2) {
		cout << "|" << setw(6) << p << "|";
		for (double n = nMin; n <= nMax; n *= 2)
		{
			double nsquare = n * n;
			double serialTime = nsquare;
			double parallelTime = nsquare / p + log2(p);
			double efficiency = round(serialTime / parallelTime / p * 100) / 100;
			cout << setw(6) << efficiency << "|";
		}
		printHorizontalLine();
	}
}

int main()
{
	Question1();
}